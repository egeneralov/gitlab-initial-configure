import os
from time import sleep

from selenium import webdriver


class Gitlab:
  
  def __init__(self, url, password):
    self.driver = webdriver.Firefox()
    try:
      self.init(url=url, password=password)
    except:
      pass
    self.login(url=url, password=password)
  
  
  def __del__(self):
    self.driver.quit()
  
  
  def init(self, url, password):
    self.driver.get(url)
    self.driver.find_element_by_xpath('//input[@id="user_password"]').send_keys(password)
    self.driver.find_element_by_xpath('//input[@id="user_password_confirmation"]').send_keys(password)
    self.driver.find_element_by_xpath('//input[@type="submit"]').click()
    
    
  def login(self, url, password):
    self.driver.get(url)
    self.driver.find_element_by_xpath('//input[@id="user_login"]').send_keys('root')
    self.driver.find_element_by_xpath('//input[@id="user_password"]').send_keys(password)
    self.driver.find_element_by_xpath('//input[@type="submit"]').click()


if __name__ == '__main__':
  Gitlab(**{
    'url': os.environ['URL'],
    'password': os.environ['PASSWORD']
  })
