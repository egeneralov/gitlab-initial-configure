FROM debian:9

ENV HOME=/root USER=root DISPLAY=:0.0 DRIVER=https://github.com/mozilla/geckodriver/releases/download/v0.23.0/geckodriver-v0.23.0-linux64.tar.gz

ADD vncpasswd /root/

RUN apt-get update -q && \
    apt-get install -yq python-pip tightvncserver curl xterm firefox-esr expect && \
    curl -sL ${DRIVER} | tar xzvf - -C /bin && \
    /root/vncpasswd && \
    apt-get purge curl expect -yq && \
    apt-get autoclean -yq && \
    apt-get autoremove -yq && \
    apt-get clean -yq && \
    mkdir -p /root/.vnc/

ADD xstartup /root/.vnc/

RUN pip install selenium

WORKDIR /app/
ADD . .


CMD vncserver :0 && python /app/gitlab_auto_configure.py && cat /root/.vnc/*:0.log
